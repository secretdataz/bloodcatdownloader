﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace BloodcatDownload
{
    public partial class FormDownloading : Form
    {
        public FormDownloading(bool auto, String mapid)
        {
            InitializeComponent();
            autoInstall = auto;
            this.map = mapid;
            label1.Text = "Downloading - " + mapid + ".osz";
            startDL();
        }
        public bool autoInstall = false;
        public String map = "";
        public void startDL()
        {
            WebClient client = new WebClient();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
            client.DownloadFileAsync(new Uri("http://bloodcat.com/osu/m/" + map), map + ".osz");
        }
        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            label1.Text = "Downloading - " + map + ".osz - " + percentage.ToString("###.##");
            progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());
        }
        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            label1.Text = "Completed";
            if (autoInstall)
            {
                System.Diagnostics.Process.Start(@map+".osz");
            }
            this.Hide();
        }
        
    }
}
